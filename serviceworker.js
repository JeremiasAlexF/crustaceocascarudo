// Nombre que tendrá la cache del sitio. En este caso va versionado por fecha y hora, lo cual es opcional
var staticCacheName = "Cascarudo-v" + new Date().getTime();

// La ruta de los archivos que quiero cachear
var filesToCache = [
    '/',
    '/offline',
    '/common-static/css/estilos.css',
    '/static/imagenes/logo.png',
];

// Cache al instalar (se mantiene el código original)
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Limpiar cache al activar (se mantiene código original)
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("mis-perris-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Servicio desde el cache (código actualizado para que cachee todos los elementos con clone)
self.addEventListener("fetch", event => {
    event.respondWith(
        fetch(event.request).then((result)=>{
            return caches.open(staticCacheName).then(function(c) {
                c.put(event.request.url, result.clone())
                return result;
            })
        }).catch(function(e){
            return caches.match(event.request)
        })
    )
});

