from rest_framework import viewsets, permissions
from .serializers import UsuariosListaSerializer, ClienteListaSerializer

#Modelos que necesito llamar
from usuarios.models import Usuarios
from cliente.models import Cliente



class ClienteListaJson(viewsets.ModelViewSet):

    queryset = Cliente.objects.all().order_by('rut')
    serializer_class = ClienteListaSerializer
    #permisson_classes = [permissions.IsAuthenticated]




class UsuariosListaJson(viewsets.ModelViewSet):
    
    queryset = Usuarios.objects.all().order_by('correo')
    serializer_class = UsuariosListaSerializer
    #permisson_classes = [permissions.IsAuthenticated]

