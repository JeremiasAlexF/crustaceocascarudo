from rest_framework import serializers
#Modelos que se usan
from usuarios.models import Usuarios
from cliente.models import Cliente


class ClienteListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cliente
        fields = ['nombre_y_apellido', 'rut', 'correo', 'telefono', 'Direccion']


class UsuariosListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Usuarios
        fields = '__all__'
