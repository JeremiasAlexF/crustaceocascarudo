from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'usuarios', views.UsuariosListaJson)
router.register(r'cliente', views.ClienteListaJson)

urlpatterns = [
    path('', include(router.urls)),
]

