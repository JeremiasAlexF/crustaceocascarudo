from django import forms
from .models import Usuarios
from django.contrib.auth.forms import UserCreationForm

class UsuariosForm(forms.ModelForm):

    class Meta:
        model = Usuarios
        fields = ('nombre_y_apellido', 'rut', 'correo', 'telefono', 'fecha', 'productoFavorito', 'puntuacion_del_producto',
                    'Direccion')
            



class CustomUserForm(UserCreationForm):
    pass