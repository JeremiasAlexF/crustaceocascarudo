from django.urls import path
from . import views 
from .views import inicio, menu, registro_usuario

urlpatterns = [
    
    path('', views.usuarios_form, name = 'usuarios_form'),
    path('inicio/', inicio, name = "inicio"),
    path('menu/', menu, name = 'menu'),
    path('usuarios/<int:pk>/', views.usuarios_respuesta, name = 'usuarios_res'),   
    path('registro/', registro_usuario, name = 'registro_usuario')
    
]

