from django.db import models
from django.utils import timezone


puntuacion = (
   ('Select','Seleccione una opción'),
   ('Uno','1'),
   ('Dos','2'),
   ('Tres','3'),
   ('Cuatro','4'),
   ('Cinco','5')
)

Producto = (

   ('1','Super combo cascarudo'),
   ('2','Pizza Cascaruda napolitana'),
   ('3','Completo Cascarudo Italiano')

)

class Usuarios(models.Model):
   nombre_y_apellido = models.CharField(max_length = 100)
   rut = models.CharField(max_length = 100)
   correo = models.CharField(max_length = 200)
   telefono = models.CharField(max_length = 100)
   fecha = models.DateTimeField(default = '2020-12-12')
   productoFavorito = models.CharField(max_length = 25, choices = Producto)
   puntuacion_del_producto = models.CharField(max_length = 10, choices = puntuacion, default = 'Select')
   Direccion = models.TextField(max_length = 200)
    
   def __str__(self):
      return self.correo
   objects = models.Manager()


