from django.shortcuts import render, get_object_or_404, redirect
from .forms import UsuariosForm, CustomUserForm
from .models import Usuarios
from cliente.models import Cliente
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, authenticate

@permission_required('usuarios.add_usuarios')
def usuarios_form(request):
    if request.method == "POST":
        form = UsuariosForm(request.POST)
        if form.is_valid():
            usuario = form.save(commit=False)
            usuario.save()
            return redirect('usuarios_res', pk=usuario.pk)
    else:
        form = UsuariosForm()
        cliente = Cliente.objects.all()
    return render(request, 'usuarios/usuarios_form.html', {'form': form, })


def usuarios_respuesta(request, pk):
    usuarios = get_object_or_404(Usuarios, pk=pk)
    return render(request, 'usuarios/usuarios_respuesta.html', {'usuarios': usuarios })



def inicio(request):
    return render(request, 'usuarios/Inicio.html')


def menu(request):
    return render(request, 'usuarios/Menú.html')


def registro_usuario(request):
    data = {
        'form':CustomUserForm()
    }

    if request.method == "POST":
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()

            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username = username, password= password)
            login(request, user)
            return redirect(to = 'inicio')
    return render(request, 'registration/registrar.html', data)