function validar(){
  ("#formulario").validate({
    rules: {
      nombre:{
        required:true
      },
      rut:{
        required:true
      },
      correo:{
        required:true
      },
      fono:{
        required:true
      },
      fecha:{
        required:true
      },
      comida:{
        required:true
      },
      estado:{
        required:true
      },
    messages:{
      nombre:{
        required:'debe ingresar el nombre!',
        minlength:'5 caracteres minimo'
      },
      rut:{
        required:'debe ingresar el rut!',
        minlength:'8 caracteres minimo',
        maxlegth: "10 caracteres maximo"
      },
      correo:{
        required:'debe ingresar el correo!',
      },
      fono:{
        required:'debe ingresar el telefono!',
        minlength:'9 digitos'
      },

      comida:{
        required:'debe ingresar la comida!'

      },
      fecha:{
        required:'debe ingresar la fecha!'
      },

      estado:{
        required:'debe ingresar estado!',
      }
    }

  }
})
}