from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render, redirect
from .forms import ClienteAdd
from .models import Cliente

# Create your views here.

def cliente_add(request):
    if request.method == "POST":
        form = ClienteAdd(request.POST, request.FILES)
        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()
            return redirect('cliente_detail', pk=cliente.pk)
    else:
        form = ClienteAdd()
    return render(request, 'cliente/cliente_add.html', {'form': form})


def cliente_list(request):
    return render(request, 'cliente/cliente_list.html', {})

def cliente_detail(request, pk):
    cliente = get_object_or_404(Cliente, pk=pk)
    return render(request, 'cliente/cliente_detail.html', {'cliente': cliente})


def cliente_edit(request, pk):
    cliente = get_object_or_404(Cliente, pk=pk)
    if request.method == "POST":
        form = ClienteAdd(request.POST, instance=cliente)
        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()
            return redirect('cliente_list')
    else:
        form = ClienteAdd(instance=cliente)
    return render(request, 'cliente/cliente_edit.html', {'form': form})

def cliente_delete(request, pk):  
    cliente = Cliente.objects.get(pk=pk)  
    cliente.delete()  
    return redirect('cliente_list')
