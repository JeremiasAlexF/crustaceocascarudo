from django import forms
from .models import Cliente

class ClienteAdd(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = ('nombre_y_apellido','rut','correo','telefono','Direccion')
