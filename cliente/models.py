from django.db import models

# Create your models here.

class Cliente(models.Model):
    nombre_y_apellido = models.CharField(max_length = 100)
    rut = models.CharField(max_length = 100)
    correo = models.CharField(max_length = 200)
    telefono = models.CharField(max_length = 100)
    Direccion = models.TextField(max_length = 200)
    
    
    def __str__(self):
        return self.rut
    
    objects = models.Manager()