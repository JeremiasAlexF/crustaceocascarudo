from django.urls import path
from . import views

urlpatterns = [
    path('add', views.cliente_add, name='cliente_add'),
    path('<int:pk>/', views.cliente_detail, name='cliente_detail'),
    path('<int:pk>/edit/', views.cliente_edit, name='cliente_edit'),
]
