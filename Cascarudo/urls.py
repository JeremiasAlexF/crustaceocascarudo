from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('usuarios.urls')),
    path('accounts/', include('django.contrib.auth.urls')), 
    path('cliente/', include('cliente.urls')),
    path('api/', include('api.urls')),
    path('', include('pwa.urls')),

]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_header  = "Administración de Crustaceo Cascarudo"
admin.site.site_title = "Crustaceo Cascarudo " 